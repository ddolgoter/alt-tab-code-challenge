'use strict';

let db = require("../database");
let Q = require("q");

exports.getUser = function (data) {
	var deferred = Q.defer();
	var collection = db.connection().collection('users');
	//in a real application password hash-codes would be compared instead of actual passwords
	collection.findOne({"email": data.email, "password": data.password}, {_id: 0},
		function (err, doc) {
			if (err)
				deferred.reject(err);
			else {
				deferred.resolve(doc);
			}
		});

	return deferred.promise;
};

// Validates password and email
var validateUser = function (email, password) {
	if (typeof email !== "string" || !email) // normally would validate that it is a valid email string
		return false;

	if (typeof password !== "string" || !password) // usually is checked for a string of a given complexity etc
		return false;

	return true;
}

exports.registerUser = function (user) {
	var deferred = Q.defer();

	if (!validateUser(user.email, user.password)) {
		deferred.reject();
		return deferred.promise;
	}

	var collection = db.connection().collection('users');

	addIfEmailDoesntExist(user, collection, deferred);

	return deferred.promise;
};

var addIfEmailDoesntExist = function (user, collection, deferred) {
	collection.findOne({email: user.email}, function (err, doc) {
		if (doc)
			deferred.reject({message: "email exists"});
		else
			insertUserIntoDatabase(user, collection, deferred);
	});
};

var insertUserIntoDatabase = function (user, collection, deferred) {
	//A real application would have hash-codes of passwords instead of actual passwords saved
	var newUser = {name: user.name, email: user.email, password: user.password};
	collection.insertOne({name: user.name, email: user.email, password: user.password},
		function (err, result) {
			if (err)
				deferred.reject({message: "database error"});
			else
				deferred.resolve(newUser);
		});
};