'use strict';

let express = require('express');
let app = express();

/* Your code */
let path = require('path');
let bodyParser = require('body-parser');
let logger = require('morgan');

// view engine setup
app.set('views', path.join(__dirname, 'app_client'));
// a trick to make express work with regular .html files
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(logger('dev'));

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});

let apiRoutes = require('./routes/api');
let homeRoutes = require('./routes/home');
app.use('/', homeRoutes);
app.use('/api', apiRoutes);

//Ensures that the application serves requests only after a database connection has been made active
require("./database.js").connect()
	.then(function () {
		app.listen(3000, function () {
			console.log("listening on port 3000");
		});
	});


module.exports = app;
