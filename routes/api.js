var express = require('express');
var router = express.Router();
var btoa = require('btoa');
var atob = require('atob');
var userService = require("../services/user");


router.post('/login', function (req, res, next) {
	userService.getUser(req.body)
		.then(function (user) {
			user.exp = Date.now(); //set this so session doesn't expire in browser
			var data = {token: "data." + btoa(JSON.stringify(user))};
			res.status(200);
			res.send(data);
		});

});


router.post('/register', function (req, res, next) {
	userService.registerUser(req.body)
		.then(function (user) {
			user.exp = Date.now();
			var data = {token: "Bearer ." + btoa(JSON.stringify(user))};
			res.status(201); //created
			res.send(data);
		})
		.catch(function () {
			res.status(400);
			res.send(null);
		});
});

router.get('/profile', function (req, res, next) {
	var token = req.headers['authorization'];

	if (!token) {
		res.status(401);
		res.send();
	} else {
		var payload = token.split('.')[1];
		payload = atob(payload);
		//probably a database call is expected here to fetch user profile data
		//but the token already has everything
		var user = JSON.parse(payload);
		res.send(user);
	}

});

router.post('/logout', function (req, res, next) {
	res.send("{}");
});


module.exports = router;
