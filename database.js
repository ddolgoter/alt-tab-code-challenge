var MongoClient = require('mongodb').MongoClient;
var Q = require('q');
var dbConnection = null;
// Connection URL
var url = 'mongodb://localhost:27017/userdb_alt_tab';

module.exports.connect = function () {
	var deferred = Q.defer();
	// Use connect method to connect to the server
	MongoClient.connect(url, function (err, db) {
		console.log("Connected successfully to MongoDB");
		//Stale data could fail some tests
		db.dropDatabase();
		dbConnection = db;
		deferred.resolve();
	});
	return deferred.promise;
};

module.exports.connection = function () {
	return dbConnection;
};

